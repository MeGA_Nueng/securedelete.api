﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Repositories.Interface
{
    public interface ISecureDeleteRepository
    {
        void InsertDeleteLog(string filename, string fullPath, bool status, string ipAddress, string macAddress, string description, int userId);
    }
}
