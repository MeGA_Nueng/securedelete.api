﻿using DeleteFile.Api.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Repositories.Interface
{
    public interface IUserRepository
    {
        User GetUser(string email, string pass);
    }
}
