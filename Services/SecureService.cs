﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Models;
using DeleteFile.Api.Repositories.Interface;
using DeleteFile.Api.Services.Interface;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace DeleteFile.Api.Services
{
    public class SecureService : ISecureService
    {
        private readonly ISecureDeleteRepository _secureDeleteRepository;
        public SecureService(ISecureDeleteRepository secureDeleteRepository)
        {
            _secureDeleteRepository = secureDeleteRepository;
        }
        public ResponseModel Execute(string filename, int round)
        {
            var response = new ResponseModel();
            try
            {
                if (string.IsNullOrEmpty(filename))
                {
                    response.Status = 400;
                    response.Message = "Path file is Empty";
                    return response;
                }

                if (round <= 0)
                    round = 5;

                if (File.Exists(filename))
                {
                    File.SetAttributes(filename, FileAttributes.Normal);

                    double sectors = Math.Ceiling(new FileInfo(filename).Length / 512.0);
                    byte[] dummyBuffer = new byte[512];

                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                    FileStream inputStream = new FileStream(filename, FileMode.Open);

                    for (int currentPass = 0; currentPass < round; currentPass++)
                    {
                        inputStream.Position = 0;

                        for (int sectorsWritten = 0; sectorsWritten < sectors; sectorsWritten++)
                        {
                            rng.GetBytes(dummyBuffer);
                            inputStream.Write(dummyBuffer, 0, dummyBuffer.Length);
                        }
                    }

                    inputStream.SetLength(0);

                    inputStream.Close();

                    var generator = new Random();
                    DateTime dt = DateTime.Now.AddDays(generator.Next(10000, 20000));
                    File.SetCreationTime(filename, dt);
                    File.SetLastAccessTime(filename, dt);
                    File.SetLastWriteTime(filename, dt);

                    File.Delete(filename);

                    response.Status = 200;
                    response.Message = "Success";
                    return response;
                }
                else
                {
                    response.Status = 404;
                    response.Message = "The file does not exist.";
                    return response;
                }
            }
            catch (Exception)
            {
                response.Status = 404;
                response.Message = "An error has occurred or the file maybe using.";
                return response;
            }
        }

        [Obsolete]
        public ResponseModel SftpExecute(string pathFile, int round, string host, int port, string username, string password)
        {
            var response = new ResponseModel();
            try
            {
                if (string.IsNullOrEmpty(pathFile))
                {
                    response.Status = 400;
                    response.Message = "Path file is Empty";
                    return response;
                }

                if (round <= 0)
                    round = 5;

                using (var sftpClient = new SftpClient(host, port, username, password))
                {
                    sftpClient.Connect();
                    sftpClient.KeepAliveInterval = TimeSpan.FromSeconds(60);
                    sftpClient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(10);
                    sftpClient.OperationTimeout = TimeSpan.FromMinutes(10);

                    if (sftpClient.Exists(pathFile))
                    {
                        double sectors = Math.Ceiling(sftpClient.ReadAllBytes(pathFile).Length / 512.0);
                        byte[] dummyBuffer = new byte[512];

                        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                        using (MemoryStream memory = new MemoryStream(dummyBuffer))
                        {
                            for (int currentPass = 0; currentPass < round; currentPass++)
                            {
                                memory.Position = 0;

                                for (int sectorsWritten = 0; sectorsWritten < sectors; sectorsWritten++)
                                {
                                    rng.GetBytes(dummyBuffer);
                                    MemoryStream dummy = new MemoryStream(dummyBuffer);
                                    sftpClient.UploadFile(dummy, pathFile, true);
                                }
                            }
                        }

                        //var generator = new Random();
                        //DateTime dt = DateTime.Now.AddDays(generator.Next(10000, 20000));
                        //sftpClient.SetLastAccessTime(pathFile, dt);
                        //sftpClient.SetLastWriteTime(pathFile, dt);

                        sftpClient.DeleteFile(pathFile);

                        sftpClient.Disconnect();

                        response.Status = 200;
                        response.Message = "Success";
                        return response;
                    }
                    else
                    {
                        response.Status = 404;
                        response.Message = "The file does not exist.";
                        return response;
                    }
                }
            }
            catch (System.Net.Sockets.SocketException)
            {
                response.Status = 401;
                response.Message = "Unable to establish the socket.";
                return response;
            }
            catch (Renci.SshNet.Common.SshAuthenticationException)
            {
                response.Status = 401;
                response.Message = "Authentication of SSH session failed.";
                return response;
            }
            catch (Exception ex)
            {
                response.Status = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public List<string> GetDirectories(string path, string searchPattern = "*", SearchOption searchOption = SearchOption.AllDirectories)
        {
            if (searchOption == SearchOption.TopDirectoryOnly)
                return Directory.GetDirectories(path, searchPattern).ToList();

            var directories = new List<string>(GetDirectories(path, searchPattern));

            for (var i = 0; i < directories.Count; i++)
                directories.AddRange(GetDirectories(directories[i], searchPattern));

            return directories;

        }

        public void InsertDeleteLog(string filename, string fullPath, bool status, string ipAddress, string macAddress, string description, int userId)
        {
            string ipV4 = null;
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    ipV4 = ip.ToString();
            }

            String firstMacAddress = NetworkInterface.GetAllNetworkInterfaces()
                .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();

            _secureDeleteRepository.InsertDeleteLog(filename, fullPath, status, ipV4, firstMacAddress, description, userId);
        }

        private List<string> GetDirectories(string path, string searchPattern)
        {
            try
            {
                return Directory.GetDirectories(path, searchPattern).ToList();
            }
            catch (UnauthorizedAccessException)
            {
                return new List<string>();
            }
        }

        [Obsolete]
        public ResponseModel SftpFolderExecute(string pathFile, int round, string host, int port, string username, string password, int userId)
        {
            var response = new ResponseModel();
            try
            {
                if (string.IsNullOrEmpty(pathFile))
                {
                    response.Status = 400;
                    response.Message = "Path file is Empty";
                    return response;
                }

                if (round <= 0)
                    round = 5;

                using (var sftpClient = new SftpClient(host, port, username, password))
                {
                    sftpClient.Connect();
                    sftpClient.KeepAliveInterval = TimeSpan.FromSeconds(60);
                    sftpClient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(15);
                    sftpClient.OperationTimeout = TimeSpan.FromMinutes(15);

                    var res = new ResponseModel();
                    int fail = 0;
                    List<string> files = new List<string>();
                    ListDirectory(sftpClient, pathFile, ref files);

                    foreach (var file in files)
                    {
                        string[] fileNames = file.Split('/');
                        string fileName = fileNames[fileNames.Count() - 1];

                        var result = SubFolderDelete(sftpClient, file, round);
                        InsertDeleteLog(fileName, file, result, "", "", "", userId);
                    }

                    List<string> folders = new List<string>();
                    DeleteDirectoryFolder(sftpClient, pathFile, ref folders);

                    for (int i = folders.Count() - 1; i >= 0; i--)
                        sftpClient.DeleteDirectory(folders[i]);
                    sftpClient.DeleteDirectory(pathFile);

                    sftpClient.Disconnect();

                    response.Status = 200;
                    response.Message = "Success";
                    return response;
                }
            }
            catch (System.Net.Sockets.SocketException)
            {
                response.Status = 401;
                response.Message = "Unable to establish the socket.";
                return response;
            }
            catch (Renci.SshNet.Common.SshAuthenticationException)
            {
                response.Status = 401;
                response.Message = "Authentication of SSH session failed.";
                return response;
            }
            catch (Exception ex)
            {
                response.Status = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        void ListDirectory(SftpClient client, String dirName, ref List<String> files)
        {
            foreach (var entry in client.ListDirectory(dirName))
            {

                if (entry.IsDirectory && entry.Name != ".." && entry.Name != ".")
                {
                    ListDirectory(client, entry.FullName, ref files);
                }
                if (!entry.IsDirectory)
                {
                    files.Add(entry.FullName);
                }
            }
        }

        void DeleteDirectoryFolder(SftpClient client, String dirName, ref List<String> folders)
        {
            foreach (var entry in client.ListDirectory(dirName))
            {

                if (entry.IsDirectory && entry.Name != ".." && entry.Name != ".")
                {
                    folders.Add(entry.FullName);
                    DeleteDirectoryFolder(client, entry.FullName, ref folders);
                }
            }
        }

        private bool SubFolderDelete(SftpClient sftpClient, string file, int round)
        {
            try
            {
                if (sftpClient.Exists(file))
                {
                    double sectors = Math.Ceiling(sftpClient.ReadAllBytes(file).Length / 512.0);
                    byte[] dummyBuffer = new byte[512];

                    RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                    using (MemoryStream memory = new MemoryStream(dummyBuffer))
                    {
                        for (int currentPass = 0; currentPass < round; currentPass++)
                        {
                            memory.Position = 0;

                            rng.GetBytes(dummyBuffer);
                            MemoryStream dummy = new MemoryStream(dummyBuffer);
                            sftpClient.UploadFile(dummy, file, true);
                        }
                    }

                    //var generator = new Random();
                    //DateTime dt = DateTime.Now.AddDays(generator.Next(10000, 20000));
                    //sftpClient.SetLastAccessTime(pathFile, dt);
                    //sftpClient.SetLastWriteTime(pathFile, dt);

                    sftpClient.DeleteFile(file);
                    return true;
                }
                else return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
