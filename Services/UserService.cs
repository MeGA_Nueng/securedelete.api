﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Repositories.Interface;
using DeleteFile.Api.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public User GetUser(string userName, string pass)
        {
            return _userRepository.GetUser(userName, pass);
        }
    }
}
