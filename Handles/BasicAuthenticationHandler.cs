﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Models;
using DeleteFile.Api.Repositories.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace DeleteFile.Api.Handles
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IConfiguration _configuration;
        private readonly IUserRepository _userRepository;
        private readonly string _username;
        private readonly string _password;
        private readonly int _userId;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IConfiguration configuration,
            IUserRepository userRepository)
            : base(options, logger, encoder, clock)
        {
            _configuration = configuration;
            _userRepository = userRepository;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var response = new AuthorizationResponseModel()
            {
                status = "error"
            };

            //return AuthenticateResult.Fail("invalid username or password");

            if (!Request.Headers.ContainsKey("Authorization"))
            {
                Context.Response.ContentType = "application/json; charset=utf-8";
                response.message = "missing authorization header";
                Response.StatusCode = 401;
                await Context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                return AuthenticateResult.Fail("missing authorization header");
            }


            User user = null;
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
                var username = credentials[0];
                var password = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials[1].ToString()));
                user = _userRepository.GetUser(username, password);
            }
            catch
            {
                Context.Response.ContentType = "application/json; charset=utf-8";
                response.message = "invalid authorization header";
                Response.StatusCode = 401;
                await Context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                return AuthenticateResult.Fail("invalid authorization header");
            }

            if (user == null)
            {
                Context.Response.ContentType = "application/json; charset=utf-8";
                response.message = "invalid username or password";
                Response.StatusCode = 401;
                await Context.Response.WriteAsync(JsonConvert.SerializeObject(response));
                return AuthenticateResult.Fail("invalid username or password");
            }

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
            };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            response.status = "success";
            return AuthenticateResult.Success(ticket);
        }

        public UserModel IsAuthorizedUser(string Username, string Password)
        {
            // In this method we can handle our database logic here...  
            if (Username == _username && Password == _password)
            {
                return new UserModel()
                {

                    FirstName = Username,
                    LastName = Username,
                    Id = 1,
                    Username = Username,
                    Password = null
                };
            }
            else return null;
        }

    }
}
