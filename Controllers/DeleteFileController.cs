﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Models;
using DeleteFile.Api.Services.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DeleteFile.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DeleteFileController : ControllerBase
    {
        private readonly ISecureService _secureService;
        private readonly IUserService _userService;
        private readonly IOptionsMonitor<AuthenticationSchemeOptions> _optionsMonitor;


        public DeleteFileController(ISecureService secureService, IUserService userService, IOptionsMonitor<AuthenticationSchemeOptions> options)
        {
            _secureService = secureService;
            _userService = userService;
            _optionsMonitor = options;
        }

        [HttpDelete("sftpFile")]
        public IActionResult sftpDeleteFile(string pathFile, int round, string host, int port, string username, string password)
        {
            var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
            var acc = credentials[0];
            var pass = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials[1].ToString()));
            User user = _userService.GetUser(acc, pass);

            string fullPath = pathFile;
            string[] fileNames = fullPath.Split('/');
            string fileName = fileNames[fileNames.Count() - 1];
            var res = _secureService.SftpExecute(pathFile, round, host, port, username, password);
            if (res.Status == 200)
                _secureService.InsertDeleteLog(fileName, fullPath, true, "", "", "", user.Id);
            else
                _secureService.InsertDeleteLog(fileName, fullPath, false, "", "", "", user.Id);

            switch (res.Status)
            {
                case 200: return Ok(res);
                case 400: return BadRequest(res);
                case 404: return NotFound(res);
                case 401: return Unauthorized(res);
                default: return StatusCode(500, res);
            }
        }

        [HttpDelete("File")]
        public IActionResult DeleteFile(string pathFile, int round)
        {
            var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
            var username = credentials[0];
            var password = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials[1].ToString()));
            User user = _userService.GetUser(username, password);

            string fullPath = pathFile;
            string[] fileNames = fullPath.Split('\\');
            string fileName = fileNames[fileNames.Count() - 1];
            var res = _secureService.Execute(pathFile, round);
            if (res.Status == 200)
                _secureService.InsertDeleteLog(fileName, fullPath, true, "", "", "", user.Id);
            else
                _secureService.InsertDeleteLog(fileName, fullPath, false, "", "", "", user.Id);

            switch (res.Status)
            {
                case 200: return Ok(res);
                case 400: return BadRequest(res);
                case 404: return NotFound(res);
                case 401: return Unauthorized(res);
                default: return StatusCode(500, res);
            }
        }

        [HttpDelete("sftpFolder")]
        public IActionResult sftpDeleteFolder(string pathFile, int round, string host, int port, string username, string password)
        {
            var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
            var acc = credentials[0];
            var pass = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials[1].ToString()));
            User user = _userService.GetUser(acc, pass);

            var res = _secureService.SftpFolderExecute(pathFile, round, host, port, username, password,user.Id);

            switch (res.Status)
            {
                case 200: return Ok(res);
                case 400: return BadRequest(res);
                case 404: return NotFound(res);
                case 401: return Unauthorized(res);
                default: return StatusCode(500, res);
            }
        }

        [HttpDelete("Folder")]
        public IActionResult DeleteFolder(string pathFile, int round)
        {
            var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
            var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
            var username = credentials[0];
            var password = Convert.ToBase64String(Encoding.UTF8.GetBytes(credentials[1].ToString()));
            User user = _userService.GetUser(username, password);

            var res = new ResponseModel();
            int fail = 0;
            List<string> root = new List<string>();
            root.Add(pathFile);
            List<string> exists = _secureService.GetDirectories(pathFile);
            root.AddRange(exists);

            List<string> subFolders = new List<string>();
            foreach (var subDir in root)
            {
                string[] files = Directory.GetFiles(subDir);
                if (files.Count() > 0)
                {
                    foreach (var sDelete in files)
                    {
                        string fullPath = sDelete;
                        string[] fileNames = fullPath.Split('\\');
                        string fileName = fileNames[fileNames.Count() - 1];
                        res = _secureService.Execute(sDelete, round);
                        if (res.Status == 200)
                        {
                            _secureService.InsertDeleteLog(fileName, fullPath, true, "", "", "", user.Id);
                        }
                        else
                        {
                            fail++;
                            _secureService.InsertDeleteLog(fileName, fullPath, false, "", "", "", user.Id);
                        }
                    }
                }
                if (fail == 0)
                    subFolders.Add(subDir);
            }
            for (int i = subFolders.Count() - 1; i >= 0; i--)
                Directory.Delete(subFolders[i]);

            switch (res.Status)
            {
                case 200: return Ok(res);
                case 400: return BadRequest(res);
                case 404: return NotFound(res);
                case 401: return Unauthorized(res);
                default: return StatusCode(500, res);
            }
        }

    }
}
