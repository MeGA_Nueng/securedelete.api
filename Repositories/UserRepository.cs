﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly CSPSecureDeleteLogContext _context;
        public UserRepository(CSPSecureDeleteLogContext context)
        {
            _context = context;
        }

        public User GetUser(string email, string pass)
        {
            return _context.Users.FirstOrDefault(x => x.Email == email && x.Password == pass);
        }
    }
}
