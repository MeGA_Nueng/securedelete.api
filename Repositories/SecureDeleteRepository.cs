﻿using DeleteFile.Api.EntityFramework;
using DeleteFile.Api.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Repositories
{
    public class SecureDeleteRepository : ISecureDeleteRepository
    {
        private readonly CSPSecureDeleteLogContext _context;
        public SecureDeleteRepository(CSPSecureDeleteLogContext context)
        {
            _context = context;
        }
        public void InsertDeleteLog(string filename, string fullPath, bool status, string ipAddress, string macAddress, string description, int userId)
        {
            try
            {
                var model = new DeleteFileLog()
                {
                    FileName = filename,
                    FullDirectory = fullPath,
                    IsSuccess = status,
                    IpAddress = ipAddress,
                    MacAddress = macAddress,
                    Description = description,
                    CreatedDate = DateTime.Now,
                    CreatedBy = userId
                };
                _context.DeleteFileLogs.Add(model);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                //Nlogger
                throw;
            }
        }
    }
}
