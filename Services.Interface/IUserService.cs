﻿using DeleteFile.Api.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Services.Interface
{
    public interface IUserService
    {
        User GetUser(string userName, string pass);
    }
}
