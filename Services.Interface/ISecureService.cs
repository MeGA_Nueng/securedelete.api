﻿using DeleteFile.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeleteFile.Api.Services.Interface
{
    public interface ISecureService
    {
        ResponseModel Execute(string filename, int round);
        ResponseModel SftpExecute(string pathFile, int round, string host, int port, string username, string password);
        ResponseModel SftpFolderExecute(string pathFile, int round, string host, int port, string username, string password,int userId);
        List<string> GetDirectories(string filename, string searchPattern = "*", SearchOption searchOption = SearchOption.AllDirectories);
        void InsertDeleteLog(string filename, string fullPath, bool status, string ipAddress, string macAddress, string description, int userId);
    }
}
