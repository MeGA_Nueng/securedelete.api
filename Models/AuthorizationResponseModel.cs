﻿
namespace DeleteFile.Api.Models
{
    public class AuthorizationResponseModel
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}
