﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DeleteFile.Api.EntityFramework
{
    public partial class DeleteFileLog
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string FullDirectory { get; set; }
        public bool IsSuccess { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
